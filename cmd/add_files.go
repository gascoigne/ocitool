/*
Copyright © 2021 Tom Gascoigne <tom@gascoigne.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/ocitool/ocitool"
)

var appendFilesCmd = &cobra.Command{
	Use:       "append-file image dest src=dest [src=dest]...",
	Short:     "Build a layer from input files and append it to an image",
	Long:      ``,
	Args:      cobra.MinimumNArgs(3),
	ValidArgs: []string{"image", "destination", "src=dest"},
	Run: func(cmd *cobra.Command, args []string) {
		base := args[0]
		dest := args[1]
		files := args[2:]

		mappings := make([]ocitool.FileMapping, len(files))
		for i := range files {
			var err error
			mappings[i], err = parseFileArg(files[i])
			if err != nil {
				cmd.Println(err)
				os.Exit(1)
			}
		}

		config, err := parseCommonFlags(cmd)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}

		ctx := context.Background()
		err = ocitool.AppendFiles(ctx, base, dest, config, mappings)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}
	},
}

func parseFileArg(arg string) (ocitool.FileMapping, error) {
	parts := strings.Split(arg, "=")
	if len(parts) != 2 {
		return ocitool.FileMapping{}, errors.Errorf("invalid file argument '%v'", arg)
	}

	return ocitool.FileMapping{
		Src:  parts[0],
		Dest: parts[1],
	}, nil
}

func init() {
	rootCmd.AddCommand(appendFilesCmd)

	addCommonFlags(appendFilesCmd)
}
