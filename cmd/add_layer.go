/*
Copyright © 2021 Tom Gascoigne <tom@gascoigne.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/ocitool/ocitool"
)

// appendCmd represents the addLayer command
var appendCmd = &cobra.Command{
	Use:       "append image dest layer.tar [layer.tar]...",
	Short:     "Append one or more layers to a given base image",
	Long:      ``,
	Args:      cobra.MinimumNArgs(3),
	ValidArgs: []string{"image", "destination", "layer"},
	Run: func(cmd *cobra.Command, args []string) {
		base := args[0]
		dest := args[1]
		layers := args[2:]

		config, err := parseCommonFlags(cmd)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}

		ctx := context.Background()
		err = ocitool.AppendLayers(ctx, base, dest, config, layers)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(appendCmd)

	addCommonFlags(appendCmd)
}
