module gitlab.com/gascoigne/ocitool

go 1.16

require (
	github.com/google/go-containerregistry v0.5.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.1.3
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
)
