/*
Copyright © 2021 Tom Gascoigne <tom@gascoigne.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package ocitool

import (
	"archive/tar"
	"context"
	"io"
	"os"
	"path/filepath"

	containerv1 "github.com/google/go-containerregistry/pkg/v1"
	v1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	"github.com/google/go-containerregistry/pkg/v1/tarball"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

func AppendLayers(ctx context.Context, base, dest string, config *ImageConfig, layerPaths []string) error {
	image, err := loadImage(base)
	if err != nil {
		return err
	}

	layers := make([]containerv1.Layer, len(layerPaths))
	for i, p := range layerPaths {
		logrus.Infof("loading layer %v", p)
		layers[i], err = tarball.LayerFromFile(p)
		if err != nil {
			return err
		}
	}

	image, err = mutate.AppendLayers(image, layers...)
	if err != nil {
		return err
	}

	err = saveImage(dest, config, image)
	if err != nil {
		return err
	}

	return nil
}

type FileMapping struct {
	Src  string
	Dest string
}

func AppendFiles(ctx context.Context, base, dest string, config *ImageConfig, files []FileMapping) error {
	image, err := loadImage(base)
	if err != nil {
		return err
	}

	pipeReader, pipeWriter := io.Pipe()
	layerCh := make(chan v1.Layer, 1)

	group, _ := errgroup.WithContext(ctx)

	group.Go(func() error {
		defer pipeReader.Close()

		layer, err := tarball.LayerFromReader(pipeReader)
		if err != nil {
			return err
		}

		layerCh <- layer
		return nil
	})

	group.Go(func() error {
		defer pipeWriter.Close()

		return tarFromFiles(files, pipeWriter)
	})

	err = group.Wait()
	if err != nil {
		return err
	}

	image, err = mutate.AppendLayers(image, <-layerCh)
	if err != nil {
		return err
	}

	err = saveImage(dest, config, image)
	if err != nil {
		return err
	}

	return nil
}

func tarFromFiles(files []FileMapping, dest io.Writer) error {
	tw := tar.NewWriter(dest)
	for _, file := range files {
		logrus.Infof("adding file %v", file.Dest)

		fi, err := os.Stat(file.Src)
		if err != nil {
			return errors.Wrapf(err, "unable to stat input file '%v'", file.Src)
		}

		hdr, err := tar.FileInfoHeader(fi, file.Dest)
		if err != nil {
			return errors.Wrapf(err, "unable to build tar header for '%v'", file.Src)
		}
		hdr.Name = filepath.ToSlash(file.Dest)

		err = tw.WriteHeader(hdr)
		if err != nil {
			return errors.Wrapf(err, "unable to write tar header for '%v'", file.Src)
		}

		fd, err := os.Open(file.Src)
		if err != nil {
			return errors.Wrapf(err, "unable to read '%v'", file.Src)
		}
		defer fd.Close()

		_, err = io.Copy(tw, fd)
		if err != nil {
			return errors.Wrapf(err, "unable to copy '%v'", file.Src)
		}

		err = fd.Close()
		if err != nil {
			return errors.Wrapf(err, "unable to close '%v'", file.Src)
		}
	}

	err := tw.Close()
	if err != nil {
		return errors.Wrap(err, "unable to finalize tar layer")
	}

	return nil
}
