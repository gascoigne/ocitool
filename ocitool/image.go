/*
Copyright © 2021 Tom Gascoigne <tom@gascoigne.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package ocitool

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	containerv1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/daemon"
	"github.com/google/go-containerregistry/pkg/v1/empty"
	"github.com/google/go-containerregistry/pkg/v1/remote"
	"github.com/google/go-containerregistry/pkg/v1/tarball"
	"github.com/sirupsen/logrus"
)

type ImageType string

const (
	ImageTypeRemote       ImageType = "docker"
	ImageTypeTarball      ImageType = "tar"
	ImageTypeDockerDaemon ImageType = "docker-daemon"
	ImageTypeScratch      ImageType = "scratch"
)

func parseImageRef(ref string) (ImageType, string, error) {
	if ref == "scratch" {
		return ImageTypeScratch, "", nil
	}

	parts := strings.SplitN(ref, ":", 2)
	if len(parts) != 2 {
		return "", "", fmt.Errorf("unable to parse image ref: %v", ref)
	}

	ty := ImageType(parts[0])
	switch ty {
	case ImageTypeRemote:
		fallthrough
	case ImageTypeTarball:
		fallthrough
	case ImageTypeDockerDaemon:

	default:
		return "", "", fmt.Errorf("invalid image type: %v", ty)
	}

	return ty, parts[1], nil
}

func loadImage(imageRefStr string) (image containerv1.Image, err error) {
	logrus.Infof("loading image %v", imageRefStr)
	ty, refRaw, err := parseImageRef(imageRefStr)
	if err != nil {
		return nil, err
	}

	if ty == ImageTypeScratch {
		// Cannot ParseReference because refRaw is empty
		return empty.Image, nil
	}

	imageRef, err := name.ParseReference(refRaw)
	if err != nil {
		return nil, err
	}

	switch ty {
	case ImageTypeRemote:
		image, err = remote.Image(imageRef, remote.WithAuthFromKeychain(authn.DefaultKeychain))
		if err != nil {
			return nil, err
		}

	case ImageTypeTarball:
		image, err = tarball.ImageFromPath(imageRef.String(), nil)
		if err != nil {
			return nil, err
		}

	case ImageTypeDockerDaemon:
		image, err = daemon.Image(imageRef)
		if err != nil {
			return nil, err
		}

	default:
		panic("unreachable")
	}

	return image, nil
}

func saveImage(imageRefStr string, config *ImageConfig, image containerv1.Image) error {
	logrus.Infof("saving image %v", imageRefStr)
	image, err := applyConfig(image, config)
	if err != nil {
		return err
	}

	ty, refRaw, err := parseImageRef(imageRefStr)
	if err != nil {
		return err
	}

	imageRef, err := name.ParseReference(refRaw)
	if err != nil {
		return err
	}

	switch ty {
	case ImageTypeRemote:
		if config.Tag != "" {
			return errors.New("cannot override tag with remote image push")
		}

		err = remote.Write(imageRef, image, remote.WithAuthFromKeychain(authn.DefaultKeychain))
		if err != nil {
			return err
		}

	case ImageTypeTarball:
		if config.Tag != "" {
			imageRef, err = name.ParseReference(config.Tag)
			if err != nil {
				return err
			}
		}

		err = tarball.WriteToFile(refRaw, imageRef, image)
		if err != nil {
			return err
		}

	case ImageTypeDockerDaemon:
		tag, err := name.NewTag(imageRef.String())
		if err != nil {
			return err
		}

		if config.Tag != "" {
			tag, err = name.NewTag(config.Tag)
			if err != nil {
				return err
			}
		}

		respStr, err := daemon.Write(tag, image)
		if err != nil {
			return err
		}

		var resp map[string]interface{}
		if respStr != "" {
			logrus.Infof("%v", respStr)
			_ = json.Unmarshal([]byte(respStr), &resp)

			if error, ok := resp["error"]; ok {
				return errors.New(error.(string))
			}
		}

	case ImageTypeScratch:
		return errors.New("cannot save to scratch")

	default:
		panic("unreachable")
	}

	return nil
}
