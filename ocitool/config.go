/*
Copyright © 2021 Tom Gascoigne <tom@gascoigne.me>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package ocitool

import (
	containerv1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
)

type ImageConfig struct {
	Tag        string
	Entrypoint []string
}

func applyConfig(image containerv1.Image, newConfig *ImageConfig) (containerv1.Image, error) {
	config, err := image.ConfigFile()
	if err != nil {
		return nil, err
	}

	if newConfig.Entrypoint != nil {
		config.Config.Entrypoint = newConfig.Entrypoint
	}

	return mutate.ConfigFile(image, config)
}
